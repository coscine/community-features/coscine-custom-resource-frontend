# Coscine Custom Resource Frontend
This project demonstrates how to use the [Coscine Static Site Generator]
to create a custom Coscine Resource Frontend that can be shared with
people that are not part of the respective Coscine project.  
All it requires is a `gitlab-ci.yml` file or the equivalent for GitHub actions.  

## Result
The resulting website can be viewed on [GitLab Pages] or on [GitHub Pages].  
Here is the example [GitHub repository](https://github.com/palomena/Coscine-Custom-Resource-Frontend).

## Instructions
### GitLab
1. [Fork this GitLab repository](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) - this creates a new GitLab repository under your name with the exact same contents as the current
2. In your new repositorys CI settings (`Settings` -> `CI/CD`) under `Variables` create three new variables with appropriate values called
    - COSCINE_API_TOKEN
    - COSCINE_PROJECT_NAME
    - COSCINE_RESOURCE_NAME
3. In the left sidebar, go to the `CI/CD` Tab -> `Schedules` and create a new schedule that runs every 24 hours 

### GitHub
1. [Create a new public empty GitHub repository](https://docs.github.com/en/repositories/creating-and-managing-repositories/creating-a-new-repository)
2. Replicate the `.github/workflows/coscine.yml` file with the full 
directory structure (.github/workflows) in your repository
3. In the repository settings under `Pages` set `Source` to `GitHub Actions`:
![img](./data/GitHub_pages_source.png)  
4. Then under `Secrets and variables` create a new repository variable called
`COSCINE_API_TOKEN` and fill it with your Coscine API Token:
![img](./data/GitHub_pages_create_variable.png)
Create two additional variables called `COSCINE_PROJECT_NAME` and `COSCINE_RESOURCE_NAME` and fill them with appropriate values.  
**Optional:**  
You can also set these values directly in the ci script `.github/workflows/coscine.yml` under `env`, but keep in mind that any passwords or tokens specified in that file are public!
![img](./data/GitHub_set_project_resource_name.png)
5. Go to the Actions Tab in the repository and select and run your new
Action to test it:
![img](./data/GitHub_run_workflow.png)

Your action now gets automatically triggered every 24 hours.
If you encounter any errors, feel free to report them by creating an issue
inside of this repository.

## Extending the website
To add pages to the website, we can create a directory with custom
markdown files in our new repository. The filenames of these markdown
files will serve as navigation menu entries in the website. The markdown
files themselves are converted to html and integrated with the rest
of the layout and theming. Specify the directory name in the ci script
to the generator with the command line option `-c or --custom`.  

## Customization
Certain parts of the website can be customized.
The command line options `--logo` and `--favicon` allow overriding
the default website logo and favicon with a custom path.  
To include custom static files such as new image files for the logo
you can use the `--static` option to specify the path for a custom
static file directory. The contents of this directory are copied
verbatim to the resulting websites static directory. Most importantly
they are copied directly after the default static files, overwriting
all default files with the same filenames in the process. Therefore
this option may be used to supply a custom stylesheet to the generator
which then replaces the original stylesheet.  
You may link to any of the custom static content in your custom markdown
files - embed images, sound files, videos or provide a downloadable
link to a file contained in the static directory.

[Coscine Static Site Generator]: https://git.rwth-aachen.de/coscine/community-features/coscine-static-site-generator
[GitLab Pages]: https://coscine.pages.rwth-aachen.de/community-features/coscine-custom-resource-frontend
[GitHub Pages]: https://palomena.github.io/Coscine-Custom-Resource-Frontend/index.html
